#ifndef TEBD_H
#define TEBD_H

#include <vector>
#include <string>
#include <complex>
using namespace std;
#include <uni10.hpp>
using namespace uni10;

class TEBD{
private:
    // MPS
    int chi;
    int size;
    int dim;
    int* stoarrInfoChi;
    int* arrInfoChi;
    UniTensor* tenGamma; // abrev: ten=tensor, mat=matrix, sto=storage, stoarr=storageArray
    Matrix* stomatLambda;
    Matrix* matLambda;

    // Hamiltonian
    UniTensor* tenHamiltonian;

    // Controller
    double epsilon;
    double delta;
    double endtime;
    int trotterNumber;
    double refinement;

public:
    TEBD(int chi, int size, int dim);
    ~TEBD();

    void SetController(double epsilon, double endtime, int trotterNumber, double refinement);
    void SetHamiltonian(string);

    void Evolve();
    void TwoSitesUpdate(int left, int right, int gateType);
    double Expectation();

    void GetProfile();
};

#endif
