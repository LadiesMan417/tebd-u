INC := $(UNI10_ROOT)/include
LIB := $(UNI10_ROOT)/lib
CC:=g++
DEBUG:=-g -fsanitize=address
FLAGS:=-O3 -m64 -std=c++11

main.e: main.o TEBD.o
	$(CC) $(FLAGS) -I$(INC) -L$(LIB) $^ -o $@ -lblas -llapack -lm -luni10
main.o: main.cpp TEBD.cpp TEBD.h
	$(CC) $(FLAGS) -I$(INC) -c $<
TEBD.o: TEBD.cpp TEBD.h
	$(CC) $(FLAGS) -I$(INC) -c $<

clean:
	rm -f *.o *.e
