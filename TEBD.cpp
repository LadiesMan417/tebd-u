#include <iostream>
#include <assert.h>
#include <math.h>
#include "TEBD.h"

TEBD::TEBD(int chi, int size, int dim)
    :chi(chi), size(size), dim(dim),
     stoarrInfoChi(new int[size+1]),
     tenGamma(new UniTensor[size]),
     stomatLambda(new Matrix[size+1]),
     tenHamiltonian(new UniTensor[size-1])
{
    for (int i=0; i<size+1; i++){
        int vertualbd;
        if (i <= size/2)
            vertualbd = (float)i <= log2((float)chi) ? pow(dim,i) : chi;
        else
            vertualbd = (float)(size-i) <= log2((float)chi) ? pow(dim,size-i) : chi;
        stoarrInfoChi[i] = vertualbd;
        //cout << i << " " << stoarrInfoChi[i] << endl;
    }
    arrInfoChi = &stoarrInfoChi[1];
    
    for (int i=0; i<size; i++){
        vector<Bond> bd;
        bd.push_back(Bond(BD_IN, arrInfoChi[i-1]));
        bd.push_back(Bond(BD_IN, dim));
        bd.push_back(Bond(BD_OUT, arrInfoChi[i]));
        UniTensor ut(CTYPE,bd,"tenGamma["+to_string(i)+"]");
        ut.randomize(CTYPE);
        tenGamma[i] = ut;
        //cout << tenGamma[i] << endl << endl;
    }

    for (int i=0; i<size+1; i++){
        Matrix mt(RTYPE, stoarrInfoChi[i], stoarrInfoChi[i], true); // diagonal
        mt.randomize(); mt.normalize();
        stomatLambda[i] = mt;
    }
    matLambda = &stomatLambda[1];
    //for (int i=-1; i<size; i++)
    //    cout << "matLambda[" << i << "]" << endl << matLambda[i] << endl << endl;
}

TEBD::~TEBD(){
    delete [] stoarrInfoChi;
    delete [] tenGamma;
    delete [] stomatLambda;
    delete [] tenHamiltonian;
}

void TEBD::SetController(double lepsilon, double lendtime, int ltrotterNumber, double lrefinement){
    epsilon = lepsilon;
    endtime = lendtime;
    trotterNumber = ltrotterNumber;
    refinement = lrefinement;
    delta = endtime/trotterNumber;
}

void TEBD::SetHamiltonian(string name){
    assert(name=="Heisenberg" || name=="transverseIsing");
    
    double elemSz[] = {0.5,   0,   0, -0.5};
    double elemSx[] = {  0, 0.5, 0.5,    0};
    double elemSp[] = {  0,   1,   0,    0};
    double elemSm[] = {  0,   0,   1,    0};
    Matrix matSz (dim, dim, elemSz);
    Matrix matSx (dim, dim, elemSx);
    Matrix matSp (dim, dim, elemSp);
    Matrix matSm (dim, dim, elemSm);
    Matrix matIdentity (dim, dim);
    matIdentity.identity();

    if (name == "transverseIsing"){
        double J = 1.0;
        double B = 1.0;
        vector<Bond> bonds(4, Bond(BD_OUT,dim));
        bonds[0].assign(BD_IN,dim); bonds[1].assign(BD_IN,dim);
        for (int i=0; i<size-1; i++){
            Matrix matHam = -J*otimes(2.*matSz,2.*matSz) +-1.0*otimes(B/2.*2.*matSx,matIdentity)
                          +-1.0*otimes(matIdentity,B/2.*2.*matSx);
            if (i==0) matHam = matHam +-1.0*otimes(B/2.*2.*matSx,matIdentity);
            if (i==size-2) matHam = matHam +-1.0*otimes(matIdentity,B/2.*2.*matSx);
            UniTensor tenHam(bonds, "transverseIsing["+to_string(i)+"]");
            tenHam.putBlock(matHam);
            tenHamiltonian[i] = tenHam;
           // cout << tenHamiltonian[i] << endl << endl;
        }
    }

    if (name == "Heisenberg"){
        double J = 1.0;
        Matrix matHam = J*(otimes(matSz,matSz) + 0.5*(otimes(matSp,matSm) + otimes(matSm,matSp)));
        vector<Bond> bonds(4, Bond(BD_OUT,dim));
        bonds[0].assign(BD_IN,dim); bonds[1].assign(BD_IN,dim);
        for (int i=0; i<size-1; i++){
            UniTensor tenHam(bonds, "Heisenberg["+to_string(i)+"]");
            tenHam.putBlock(matHam);
            tenHamiltonian[i] = tenHam;
            //cout << tenHamiltonian[i] << endl << endl;
        }
    }
}

void TEBD::Evolve(){
    cout.precision(15);
    cout.setf(ios::fixed);

    double localError = 1.0;
    double localTime = 0.0;
    double lastValue = 0.0;
    double curValue = 0.0;

    while (abs(localError) > epsilon){
        // second order Trotter decomposition, a full sweep, which 
        // may end with fast convergent, and procced to apply the
        // last half-delta gate
        for (int site = 0; site < size-1; site+=2){
            //cout << "site = " << site << endl << flush;
            TwoSitesUpdate(site, site+1, -1);
        }
        for (int t=0; t<trotterNumber; t++){
            if (t != trotterNumber-1){
                for (int site = 1; site < size-1; site+=2)
                    TwoSitesUpdate(site, site+1, 1);
                for (int site = 0; site < size-1; site+=2)
                    TwoSitesUpdate(site, site+1, 1);
            }
            else {
                for (int site = 1; site < size-1; site+=2)
                    TwoSitesUpdate(site, site+1, 1);
            }

            localTime += delta;
            lastValue = curValue;
            curValue = Expectation();
            localError = curValue - lastValue;

            if (abs(localError) < epsilon)
                break;
        }

        // apply the last half-delta gate to end a full sweep
        for (int site = 0; site < size-1; site+=2)
            TwoSitesUpdate(site, site+1, -1);
        lastValue = curValue;
        curValue = Expectation();
        localError = curValue - lastValue;
        cout << "T = " << localTime << ", E = " << curValue 
             << ", E/N = " << curValue/size << ", e = " << localError << endl;
        
        // refining
        delta /= refinement;
        trotterNumber *= refinement;
    }        
}

void TEBD::TwoSitesUpdate(int left, int right, int gateType){
    assert(left >= 0 && left < right && right <= size-1);
    
    // Contruct tenV
    
    UniTensor tenV(tenHamiltonian[left].bond());
    assert(gateType==-1 || gateType==1);
    if (gateType == 1)
        tenV.putBlock(takeExp(-delta, tenHamiltonian[left].getBlock()));
    if (gateType == -1)
        tenV.putBlock(takeExp(-delta/2.0, tenHamiltonian[left].getBlock()));
    
    // Construct tenTheta
    
    // absorb lambda into gamma
    tenGamma[left].permute(1);
    tenGamma[left].putBlock(matLambda[left-1] * tenGamma[left].getBlock());
    tenGamma[left].permute(2);
    tenGamma[left].putBlock(tenGamma[left].getBlock() * matLambda[left]);
    tenGamma[right].putBlock(tenGamma[right].getBlock() * matLambda[right]);
    
    // operate on the Hamiltonian
    int lbTheta[] = {0, 1, 2, 3};
    int lbL[] = {0, -1, -3};
    int lbR[] = {-3, -2, 3};
    int lbV[] = {1, 2, -1, -2};
    tenGamma[left].setLabel(lbL);
    tenGamma[right].setLabel(lbR);
    tenV.setLabel(lbV);
    UniTensor tenTheta = tenGamma[left] * tenV * tenGamma[right];
    tenTheta.permute(lbTheta,2);
    //tenTheta.printGraphy();
    

    // SVD
   
    vector<Matrix> vecmatSVD = tenTheta.getBlock().svd();
    //cout << vecmatSVD[1] << endl << endl;
    //cout << "site = " << left << ", norm = " << vecmatSVD[1].norm(CTYPE) << endl;


    // Truncate and re-structure
    
    double norm = vecmatSVD[1].resize(arrInfoChi[left],arrInfoChi[left]).norm();
    vecmatSVD[1] *= (1.0/norm);
    for (size_t i=0; i<matLambda[left].elemNum(); i++)
        matLambda[left][i] = vecmatSVD[1](i).real();
    tenGamma[left].putBlock(vecmatSVD[0].resize(vecmatSVD[0].row(), arrInfoChi[left]));
    tenGamma[right].permute(1);
    tenGamma[right].putBlock(vecmatSVD[2].resize(arrInfoChi[left], vecmatSVD[2].col()));
    tenGamma[right].permute(2);
    Matrix invmatL = matLambda[left-1];
    for (size_t i=0; i<invmatL.elemNum(); i++)
        invmatL[i] = invmatL[i] == 0 ? 0 : (1.0 / invmatL[i]);
    Matrix invmatR = matLambda[right];
    for (size_t i=0; i<invmatR.elemNum(); i++)
        invmatR[i] = invmatR[i] == 0 ? 0 : (1.0 / invmatR[i]);
    tenGamma[left].permute(1);
    tenGamma[left].putBlock(invmatL * tenGamma[left].getBlock());
    tenGamma[left].permute(2);
    tenGamma[right].putBlock(tenGamma[right].getBlock() * invmatR);

}

void TEBD::GetProfile(){
    uni10::UniTensor::profile();
}

double TEBD::Expectation(){
    double value = 0.0;
    for (int l=0; l<size-1; l++){
        // absorve matLambda to tenGamma
        UniTensor tenV = tenHamiltonian[l];
        UniTensor tenGL = tenGamma[l];
        UniTensor tenGR = tenGamma[l+1];
        tenGL.permute(1);
        tenGL.putBlock(matLambda[l-1] * tenGL.getBlock());
        tenGL.permute(2);
        tenGL.putBlock(tenGL.getBlock() * matLambda[l]);
        tenGR.putBlock(tenGR.getBlock() * matLambda[l+1]);
        
        // contract tenGamma
        int lbL[] = {0, 1, -1};
        int lbR[] = {-1, 2, 3};
        int lbGs[] = {0, 1, 2, 3};
        tenGL.setLabel(lbL);
        tenGR.setLabel(lbR);
        UniTensor tenGs = tenGL * tenGR;
        tenGs.permute(lbGs,3);
        
        // conjugate transpose, or dual corresponding
        UniTensor tencGs = tenGs;
        tencGs.cTranspose();
        int lbcGs[] = {3, 0, 4, 5};
        tencGs.setLabel(lbcGs);
        int lbV[] = {4, 5, 1, 2};
        tenV.setLabel(lbV);
        //cout << tenGs * tenV * tencGs << endl;
        value += (tenGs * tenV * tencGs)(0).real();
    }
    //cout << value << endl;
    return value;
}


