#include <iostream>
#include <time.h>
#include <ratio>
#include <chrono>
using namespace std::chrono;
#include "TEBD.h"

int main(){

    int chi = 16;
    int size = 64;
    int dim = 2;
    clock_t start, end;
    start = clock();
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    TEBD Ising(chi, size, dim);
    Ising.SetHamiltonian("transverseIsing");
    Ising.SetController(1.e-8, 10, 1, 2);
    //Ising.TwoSitesUpdate(4, 5, 1);
    //Ising.Expectation();
    Ising.Evolve();

    //Ising.GetProfile();
    end = clock();
    cout << "CPU time used: " << (float)(end-start) / CLOCKS_PER_SEC << " seconds" << endl;
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
    cout << "Wall clock time passed: " << time_span.count() << " seconds" << endl;

    return 0;
}
